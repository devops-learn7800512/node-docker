## Some Important Commands
1. docker build.
2. docker image ls
3. docker image rm 00abb82595ab
4. docker build -t node-app-image .
5. docker ps
6. docker run -p 3000:3000 -d --name node-app-container node-app-image
7. docker exec -it 0fb4c71137c1 sh
8. docker exec -it node-app-container sh
9. docker run -v $(pwd):/app -p 3000:3000 -d --name node-app-container node-app-image
10. docker run -v $(pwd):/app:ro -v /app/node_modules -p 3000:3000 -d --name node-app-container node-app-image
11. git rm -r --cached node_modules
12. docker rm node-app-container -f
13. docker run -v $(pwd):/app:ro -v /app/node_modules --env PORT=4000 -p 3000:4000 -d --name node-app-container node-app-image
14. docker run -v $(pwd):/app:ro -v /app/node_modules --env-file ./.env -p 3000:4000 -d --name node-app-container node-app-image
15. docker rm node-app-container -fv
16. docker compose up -d
17. docker compose down or ctrl^c


## bind mount volume
docker run -v pathtofolderonlocation:pathtofolderoncontainer -p 3000:3000 -d --name node-app-container node-app-image

http://localhost:3000/
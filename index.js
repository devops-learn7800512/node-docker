import express from "express";
const app = express();
const port = process.env.PORT || 3000;

app.get("/", (req, res) => {
  res.send(`
    <div style="font-family: Arial, sans-serif; padding: 10px;">
      <h2 style="color: #333;"> Oh, Hello There! </h2>
      <p style="color: #666;"> Trying out Docker.. </p>
      <p style="color: #999;"> Listening on Port ${port} </p>
    </div>
  `);
});

app.listen(port, () => {
  console.log(`Listening on Port ${port}`);
});